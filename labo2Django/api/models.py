from django.db import models

# Create your models here.
class Alumno(models.Model):
    name = models.CharField(max_length=200)
    address = models.TextField()
    email =  models.TextField()
    age = models.IntegerField()
    sex = models.TextField()
    matFav = models.TextField()

class Acompaniante(models.Model):
	name = models.CharField(max_length=200)
	age = models.IntegerField()
	nickname = models.TextField()
	nationality = models.TextField()
	price = models.IntegerField()
	qualities = models.TextField()

class Cafizo(models.Model):
    name = models.CharField(max_length=200)
    nickName = models.TextField()
    age =  models.IntegerField()
    cafizoSex = models.TextField()
    martialArt = models.TextField()


    #def __str__(self):
     #   return self.name