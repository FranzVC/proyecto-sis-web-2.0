from tastypie.resources import ModelResource
from api.models import Alumno
from api.models import Acompaniante
from api.models import Cafizo
from tastypie.authorization import Authorization

class AlumnoResource(ModelResource):
    class Meta:
        queryset = Alumno.objects.all()
        resource_name = 'alumno'
        authorization = Authorization()

class AcompanianteResource(ModelResource):
    class Meta:
        queryset = Acompaniante.objects.all()
        resource_name = 'acompaniante'
        authorization = Authorization()

class CafizoResource(ModelResource):
    class Meta:
        queryset = Cafizo.objects.all()
        resource_name = 'cafizo'
        authorization = Authorization()
