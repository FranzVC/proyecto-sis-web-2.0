function putAlumno(id) {
    window.location.href = '/frontEnd/editAlumno.html';
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var jsonObj = JSON.parse(this.responseText);
            localStorage.setItem('alumno', JSON.stringify(jsonObj))
            console.log(jsonObj);
        }
    };
    xmlhttp.open("GET", "http://127.0.0.1:8000/api/alumno/" + id + "/", true);
    xmlhttp.send();
}

function editAlumno(){
    var alumno = JSON.parse(localStorage.getItem('alumno'));
    document.getElementById("alumno_id").value = alumno._id
    document.getElementById("alumno_name").value = alumno.name;
    document.getElementById("address").value = alumno.address;
    document.getElementById("email").value = alumno.email;
    document.getElementById("age").value = alumno.age;
    document.getElementById("sex").value = alumno.sex;
    document.getElementById("matFav").value = alumno.matFav;
}

function registerN(){
    var alumno = JSON.parse(localStorage.getItem('alumno'));
    var id = alumno.id;

    var _id = document.getElementById("alumno_id").value;
    var alumno_name = document.getElementById("alumno_name").value;
    var address =document.getElementById("address").value
    var email = document.getElementById("email").value;
    var age = document.getElementById("age").value;
    var sex = document.getElementById("sex").value
    var matFav = document.getElementById("matFav").value;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        console.log(this.status);
        if (this.readyState == 4 && this.status == 204) {
            window.location.href='/frontEnd/principal.html';
        }
    };
    xmlhttp.open("PUT", "http://127.0.0.1:8000/api/alumno/" + id + "/", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(JSON.stringify({
        "_id" : _id,
        "name" : alumno_name,
        "address" : address,
        "email" : email,
        "age" : age,
        "sex" : sex,
        "matFav" : matFav
    }))
}