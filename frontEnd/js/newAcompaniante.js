function registerAcompaniante(){
    var _id = document.getElementById("acompaniante_id").value;
    var acompaniante_name = document.getElementById("acompaniante_name").value;
    var age = document.getElementById("age").value;
    var nickname = document.getElementById("nickname").value;
    var nationality = document.getElementById("nationality").value;
    var price = document.getElementById("price").value;
    var qualities = document.getElementById("qualities").value;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 201) {
            console.log(this.responseText);
            window.location.href='/frontEnd/showAcompaniante.html'; //CAMBIAR?
        }
    };

    xmlhttp.open("POST", "http://127.0.0.1:8000/api/acompaniante/", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(JSON.stringify({
        "_id" : _id,
        "name" : acompaniante_name,
        "age" : age,
        "nickname" : nickname,
        "nationality" : nationality,
        "price" : price,
        "qualities" : qualities,
    }))
}