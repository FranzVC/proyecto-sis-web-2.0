function putAcompaniante(id) {
    window.location.href = '/frontEnd/editAcompaniante.html'; //Modificar?
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var jsonObj = JSON.parse(this.responseText);
            localStorage.setItem('acompaniante', JSON.stringify(jsonObj))
            console.log(jsonObj);
        }
    };
    xmlhttp.open("GET", "http://127.0.0.1:8000/api/acompaniante/" + id + "/", true);
    xmlhttp.send();
}

function editAcompaniante()
{
    var acompaniante = JSON.parse(localStorage.getItem('acompaniante'));
    document.getElementById("acompaniante_id").value = acompaniante._id
    document.getElementById("acompaniante_name").value = acompaniante.name;
    document.getElementById("age").value = acompaniante.age;
    document.getElementById("nickname").value = acompaniante.nickname;
    document.getElementById("nationality").value = acompaniante.nationality;
    document.getElementById("price").value = acompaniante.price;
    document.getElementById("qualities").value = acompaniante.qualities;
}

function registerA()
{
    var acompaniante = JSON.parse(localStorage.getItem('acompaniante'));
    var id = acompaniante.id;

    var _id = document.getElementById("acompaniante_id").value;
    var acompaniante_name = document.getElementById("acompaniante_name").value;
    var age = document.getElementById("age").value;
    var nickname = document.getElementById("nickname").value;
    var nationality = document.getElementById("nationality").value
    var price = document.getElementById("price").value
    var qualities = document.getElementById("qualities").value;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function()
    {
        console.log(this.status);
        if (this.readyState == 4 && this.status == 204) 
        {
            window.location.href='/frontEnd/showAcompaniante.html'; //cambiar?
        }
    };
    xmlhttp.open("PUT", "http://127.0.0.1:8000/api/acompaniante/" + id + "/", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(JSON.stringify(
    {
        "_id" : _id,
        "acompaniante_name" : acompaniante_name,
        "age" : age,
        "nickname" : nickname,
        "nationality" : nationality,
        "price" : price,
        "qualities" : qualities,
    }))
}