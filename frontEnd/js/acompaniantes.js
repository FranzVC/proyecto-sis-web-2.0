function getAcompaniantes() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var jsonObj = JSON.parse(this.responseText);
            populateTable(jsonObj.objects);
        }
    };
    xmlhttp.open("GET", "http://127.0.0.1:8000/api/acompaniante/", true);
    xmlhttp.send();
}

function populateTable(jsonObj) {
    var col = [];
    for (let index = 0; index < jsonObj.length; index++) {
        for (var key in jsonObj[index]) {
            if (col.indexOf(key) === -1) 
            {     
                    col.push(key); 
            }
        }
    }

    var table = document.createElement("table");
    table.setAttribute('class', 'table table-striped');

    var thead = document.createElement("thead")

    var th = document.createElement("th");
    th.innerHTML = "Edad"
    th.setAttribute('class', 'scope white-text');
    thead.appendChild(th);

    var th = document.createElement("th");
    th.innerHTML = "Nombre"
    th.setAttribute('class', 'scope white-text');
    thead.appendChild(th);  

    var th = document.createElement("th");
    th.innerHTML = "Nacionalidad"
    th.setAttribute('class', 'scope white-text');
    thead.appendChild(th);

    var th = document.createElement("th");
    th.innerHTML = "Apodo"
    th.setAttribute('class', 'scope white-text');
    thead.appendChild(th);

    var th = document.createElement("th");
    th.innerHTML = "Precio"
    th.setAttribute('class', 'scope white-text');
    thead.appendChild(th);

    var th = document.createElement("th");
    th.innerHTML = "Cualidades"
    th.setAttribute('class', 'scope white-text');
    thead.appendChild(th);

    var th = document.createElement("th");
    th.setAttribute('class', 'scope white-text');
    thead.appendChild(th);

    th = document.createElement("th");
    th.setAttribute('class', 'scope white-text');
    thead.appendChild(th);

    table.appendChild(thead)

    for (var i = 0; i < jsonObj.length; i++) {
        tr = table.insertRow(-1);
        var id;
        for (var j = 0; j < col.length ; j++) {
            if(col[j] === "id" || col[j] === "resource_uri")
            {
            }
            else
            {
                var cell = tr.insertCell(-1);
                cell.innerHTML = jsonObj[i][col[j]];
                cell.setAttribute('class','scope white-text');
            }
            
            if (j == 1) {
                id = jsonObj[i][col[j]];
            }
        }
        var cell = tr.insertCell(-1);
        cell.innerHTML = '<button onclick="putAcompaniante('+ id +')" class="waves-effect waves-light btn grey">Editar</button>'
        cell = tr.insertCell(-1);
        cell.innerHTML = '<button onclick="deleteAcompaniante(' + id + ')" class="waves-effect waves-light btn grey">Eliminar</button>'
    }
    document.getElementById("acompaniantes").appendChild(table);
}

function newAcompaniante() {
    window.location.href = '/frontEnd/createAcompaniante.html'; //CAMBIAR?
}
/*
function populateTable(jsonObj) {
    var col = [];
    for (let index = 0; index < jsonObj.length; index++) {
        for (var key in jsonObj[index]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }
    var table = document.createElement("table");
    table.setAttribute('class', 'table table-hover table-dark');

    var thead = document.createElement("thead")

    for (var i = 0; i < col.length - 1; i++) {
        var th = document.createElement("th");
        th.innerHTML = col[i];
        th.setAttribute('class', 'scope');
        thead.appendChild(th);
    }

    var th = document.createElement("th");
    th.innerHTML = "Editar"
    th.setAttribute('class', 'scope');
    thead.appendChild(th);

    th = document.createElement("th");
    th.innerHTML = "Eliminar"
    th.setAttribute('class', 'scope');
    thead.appendChild(th);

    table.appendChild(thead)

    for (var i = 0; i < jsonObj.length; i++) {
        tr = table.insertRow(-1);
        var id;
        for (var j = 0; j < col.length - 1; j++) {
            var cell = tr.insertCell(-1);
            cell.innerHTML = jsonObj[i][col[j]];
            if (j == 3) {
                id = jsonObj[i][col[j]];
            }
        }
        var cell = tr.insertCell(-1);
        cell.innerHTML = '<button onclick="putAcompaniante('+ id +')">Editar</button>'
        cell = tr.insertCell(-1);
        cell.innerHTML = '<button onclick="deleteAcompaniante(' + id + ')">Eliminar</button>'
    }
    document.getElementById("acompaniantes").appendChild(table);
}

function newAcompaniante() {
    window.location.href = '/frontEnd/createAcompaniante.html';
}*/